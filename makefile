all: main

CPP = g++ -std=c++11
CC = gcc
CPPFLAGS = -O3 

OBJ = shorten_Z.o main.o functions.o heuristic.o

shorten_Z.o: shorten_Z.cpp shorten_Z.hpp
	$(CPP) $(CPPFLAGS) -c $<

functions.o: functions.c functions.h
	$(CPP) $(CPPFLAGS) -c $<	

heuristic.o: heuristic.cpp heuristic.h
	$(CPP) $(CCPFLAGS) -c $<

main.o: main.cpp
	$(CPP) $(CPPFLAGS) -c $<

main: $(OBJ)
	$(CPP) $(CPPFLAGS) $(OBJ) -I/home/k1635844/igraph/include/igraph -L/home/k1635844/igraph/lib -ligraph -o $@ 
	#$(CPP) $(CPPFLAGS) $(OBJ) -I/usr/local/Cellar/igraph/0.7.1_3/include/igraph -L/usr/local/Cellar/igraph/0.7.1_3/lib/ -ligraph -o $@ 
output: main
	./main testfile sen_stringfile k
clean:
	rm *.o main *~
